from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Appointment, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "tech_name",
        "employee_number",
        "id",
    ]


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
        "model",
        # "is_vip",
        # "id",
    ]

    def default(self, obj):
        if isinstance(obj, self.model):
            return obj.vin
        return super().default(obj)


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date",
        "time",
        "reason",
        "customer_vin",
        "automobile",
        "is_vip",
        "technician",
        "id",
    ]
    encoders = {
        "Automobile": AutomobileVODetailEncoder(),
        "Technician": TechnicianEncoder()
        }

    def get_extra_data(self, o):
        return {"date": str(o.date), "time": str(o.time)}

    def default(self, obj):
        if isinstance(obj, AutomobileVO):
            return obj.vin
        if isinstance(obj, Technician):
            return obj.tech_name
        return super().default(obj)



class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date",
        "time",
        "reason",
        "customer_vin",
        "automobile",
        # "is_vip",
        "technician",
        "id",
    ]
    encoders = {
        "Automobile": AutomobileVODetailEncoder(),
        "Technician": TechnicianEncoder()
        }

    def get_extra_data(self, o):
        return {"date": str(o.date), "time": str(o.time)}

    def default(self, obj):
        if isinstance(obj, AutomobileVO):
            return obj.vin
        if isinstance(obj, Technician):
            return obj.tech_name
        return super().default(obj)


@require_http_methods(["GET", "POST"])
def api_list_appointment(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            appointments = Appointment.objects.filter(automobile=automobile_vo_id)
        else:
            appointments = Appointment.objects.all()

        if not appointments.exists():
            return JsonResponse(
                {"appointment": []},
                encoder=AppointmentListEncoder,
                safe=False
            )

        return JsonResponse(
            {"appointment": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            if AutomobileVO.objects.get(vin=content["customer_vin"]) is not None:
                automobile = AutomobileVO.objects.get(vin=content["customer_vin"])
                content["is_vip"] = True
                content["automobile"] = automobile
            else:
                content["is_vip"] = False
        except AutomobileVO.DoesNotExist:
            content["is_vip"] = False

        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
        # try:
        #     automobile_vin = content.get("automobile_vin")
        #     if automobile_vin:
        #         automobile = AutomobileVO.objects.get(vin=automobile_vin)
        #         content['automobile'] = automobile
        #         del content["automobile_vin"]
        #     else:
        #         content['automobile'] = None
        # except AutomobileVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid automobile VIN"},
        #         status=400,
        #     )

        # try:
        #     technician_id = content["technician"]
        #     technician = Technician.objects.get(id=technician_id)
        #     content['technician'] = technician
        # except Technician.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid technician id"},
        #         status=400,
        #     )

        # appointment = Appointment.objects.create(**content)
        # return JsonResponse(
        #     appointment,
        #     encoder=AppointmentListEncoder,
        #     safe=False,
        # )





@require_http_methods(["GET", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            automobile = appointment.automobile
            if automobile is None or not hasattr(automobile, 'vin'):
                is_vip = False
            else:
                is_vip = AutomobileVO.objects.filter(vin=automobile.vin).exists()
            return JsonResponse(
                {"appointment": appointment, "is_vip": is_vip},
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": f"Appointment with id {pk} does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"message": f"Appointment {pk} deleted"},
                status=200
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})



@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        if not technicians:
            return JsonResponse(
                {"error": "No technicians found"},
                status=404
            )
        else:
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianEncoder,
                safe=False
            )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse(
                {"error": str(e)},
                status=400
            )



@require_http_methods(["GET", "DELETE"])
def api_show_technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
        if request.method == "GET":
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            technician.delete()
            return JsonResponse(
                {"message": f"Technician with id {pk} has been deleted"},
                status=200,
            )
    except Technician.DoesNotExist:
        return JsonResponse(
            {"message": f"Technician with id {pk} does not exist"},
            status=404,
        )
