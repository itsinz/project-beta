# Generated by Django 4.0.3 on 2023-03-09 07:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0013_alter_appointment_customer_vin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='customer_vin',
            field=models.CharField(default='', max_length=17, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='is_vip',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='reason',
            field=models.TextField(default='', null=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='time',
            field=models.TimeField(null=True),
        ),
    ]
