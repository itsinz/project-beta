from common.json import ModelEncoder

from .models import AutomobileV0, Customer, SalesPerson, Sales



class AutomobileV0Encoder(ModelEncoder):
    model = AutomobileV0
    properties = [
        "id",
        "import_href",
        "color",
        "year",
        "vin",
        'available'
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "phone",
        "address",
        "id"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id"
    ]


class SalesEncoder(ModelEncoder):
    model = Sales
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "sale_price",
         "id"
    ]

    encoders = {
        "automobile": AutomobileV0Encoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),

    }

    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin}
