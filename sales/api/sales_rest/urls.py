# urlpatterns = [
#     # ADD THIS LINE
#     path("attendees/", api_list_attendees, name="api_create_attendees"),

#     path(
#         # UPDATE THE PARAMETER NAME
#         "conferences/<int:conference_vo_id>/attendees/",
#         api_list_attendees,
#         name="api_list_attendees",
#     ),
#     path("attendees/<int:pk>/", api_show_attendee, name="api_show_attendee"),
# ]
from django.urls import path
from . import views

urlpatterns = [
    path(
        "sales_person/",
        views.api_sales_person,
        name="api_sales_person",
    ),

    path(
    "sales_person/<int:pk>/",
    views.api_show_sales_persons,
    name="api_show_sales_persons",
),
    path(
        "customer/",
        views.api_customer,
        name="api_customer",
    ),

    path(
        "customer/<int:pk>/",
        views.api_show_customers,
        name="api_show_customers",
    ),

    path(
        "sales/",
        views.api_list_sales,
        name="api_list_sales",
    ),

    path(
        "sales/<int:pk>/",
        views.api_sales_detail,
        name="api_sales_detail",
    ),

    path(
        "automobiles/",
        views.api_automobiles,
        name="api_automobiles",
    ),
]
