from django.db import models
from django.urls import reverse




class Manufacturer(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_manufacturer", kwargs={"pk": self.pk})


class VehicleModel(models.Model):
    name = models.CharField(max_length=100)
    picture_url = models.URLField()

    manufacturer = models.ForeignKey(
        Manufacturer,
        related_name="models",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_vehicle_model", kwargs={"pk": self.pk})


class AutomobileV0(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=False, default='')
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    available = models.BooleanField(default=True)

    # model = models.ForeignKey(
    #     VehicleModel,
    #     related_name="automobiles",
    #     on_delete=models.CASCADE,
    #     default = ''
    # )


    def __str__(self):
        return self.vin

    # def get_api_url(self):
    #     return reverse("api_automobile", kwargs={"vin": self.vin})



class Customer(models.Model):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.pk})


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.pk})


class Sales(models.Model):
    automobile = models.ForeignKey(
        AutomobileV0,
        on_delete=models.CASCADE,
        related_name="sales"
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        on_delete=models.CASCADE,
        related_name="sales"
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name="sales"
    )
    sale_price = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_sales_detail", kwargs={"pk": self.pk})

    def __str__(self):
        return f"Car: {self.automobile} | Customer: {self.customer}"
