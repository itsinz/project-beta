from django.contrib import admin

# Register your models here.

from .models import AutomobileV0, Customer, SalesPerson, Sales


admin.site.register(AutomobileV0)
admin.site.register(Customer)
admin.site.register(SalesPerson)
admin.site.register(Sales)
