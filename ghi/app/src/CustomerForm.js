import { useState } from 'react';

function CustomerForm(props) {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [phone, setPhone] = useState('');
    const handlePhoneChange = (event) => {
        const value = event.target.value;
        setPhone(value);
    }

    const [address, setAddress] = useState('');
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handleFormSubmit = async (event) => {
        event.preventDefault();

        // Perform validation
        if (!name || !phone) {
            alert('Please fill in all required fields');
            return;
        }

        // Submit form data to the server
        const data = {};
        data.name = name;
        data.phone = phone;
        data.address = address;
        // console.log(data);


        const url = "http://localhost:8090/api/customer/";
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            setName("");
            setPhone("");
            setAddress("");
            await props.fetchCustomer();
        } else {
            // Display error message
            alert('Error creating sales person');
        }

    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new Customer</h1>
                    <form onSubmit={handleFormSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input
                                value={name}
                                placeholder="Name"
                                required type="text"
                                name="name"
                                id="name"
                                className="form-control"
                                onChange={handleNameChange}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={phone}
                                placeholder="Phone Number"
                                required
                                type="int"
                                name="phone"
                                id="phone"
                                className="form-control"
                                onChange={handlePhoneChange}
                            />
                            <label htmlFor="phone">Phone Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={address}
                                placeholder="Address"
                                required type="text"
                                name="address"
                                id="address"
                                className="form-control"
                                onChange={handleAddressChange}
                            />
                            <label htmlFor="address">Address</label>
                        </div>
                        <button className="btn btn-primary" type="submit">
                            Add
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
