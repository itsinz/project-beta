import React, { useEffect, useState } from 'react'


function SalesHistory(props) {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    return (
        <>
            <h1>Sales Person History</h1>
            <div className="mb-3">
                <select value={name} onChange={handleNameChange} required id="location" name="location" className="form-select">
                    <option value="">Choose a Sales Person</option>
                    {(props.sales_person || []).map((sales_person) => {
                        return (
                            <option key={sales_person.id} value={sales_person.name}>
                                {sales_person.name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Customer Name</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {(props.sales || []).filter((sale) => sale.sales_person.name === name)
                        .map((sale) => {
                            return (
                                <tr key={sale.id}>
                                    <td>{sale.sales_person.name}</td>
                                    <td>{sale.sales_person.employee_number}</td>
                                    <td>{sale.customer.name}</td>
                                    <td>{sale.automobile}</td>
                                    <td>${sale.sale_price}</td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </>
    )
}

export default SalesHistory
