import { useState, useEffect } from 'react';

function ManufacturerForm() {
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleFormSubmit = async (e) => {
        e.preventDefault();

        const data = {
            name: name,
        };

        const response = await fetch('http://localhost:8100/api/manufacturers/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });

        if (response.ok) {
            const newData = await response.json();
            // Show "View all Manufacturer" button
            const viewAllBtn = document.createElement('a');
            viewAllBtn.href = 'http://localhost:3000/manufacturers';
            viewAllBtn.className = 'btn btn-primary mt-3';
            viewAllBtn.textContent = 'View all Manufacturer';
            document.getElementById('create-manufacturer').appendChild(viewAllBtn);
        } else {
            console.log('Error creating manufacturer');
        }
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a manufacturer</h1>
                    <form id="create-manufacturer" onSubmit={handleFormSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary" type="submit">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
