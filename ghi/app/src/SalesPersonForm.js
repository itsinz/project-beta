import React, { useEffect, useState } from 'react'

function SalesPersonForm(props) {

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [employee_number, setEmployeeNumber] = useState('');
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }


    const handleFormSubmit = async (event) => {
        event.preventDefault();

        // Perform validation
        if (!name || !employee_number) {
            alert('Please fill in all required fields');
            return;
        }

        // Submit form data to the server
        const data = {};
        data.name = name;
        data.employee_number = employee_number;
        // console.log(data);


        const url = "http://localhost:8090/api/sales_person/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            setName("");
            setEmployeeNumber("");
            await props.fetchSalesperson();
        } else {
            // Display error message
            alert('Error creating sales person');
        }

    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sales Person</h1>
                    <form onSubmit={handleFormSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input
                                value={name}
                                placeholder="Name"
                                required type="text"
                                name="name"
                                id="name"
                                className="form-control"
                                onChange={handleNameChange}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={employee_number}
                                placeholder="Employee Number"
                                required
                                type="int"
                                name="employee_number"
                                id="employee_number"
                                className="form-control"
                                onChange={handleEmployeeNumberChange}
                            />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary" type="submit">
                            Add
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonForm;
