import { useState, useEffect } from 'react';

function VehicleModelForm() {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturers, setManufacturer] = useState([]);
    const [selectedManufacturer, setSelectedManufacturer] = useState("");
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const fetchManufacturers = async () => {
            const url = "http://localhost:8100/api/manufacturers/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturer(data.manufacturers);
            }
        };
        fetchManufacturers();
    }, []);


    const handleFormSubmit = async (event) => {
        event.preventDefault();

        const picture_url = pictureUrl;
        const manufacturer_id = selectedManufacturer;
        const data = {
            name,
            picture_url,
            manufacturer_id,
        };


        const modelurl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(modelurl, fetchConfig);
        if (response.ok) {
            event.target.reset();
            setName("");
            setPictureUrl("");
            setSelectedManufacturer("");
            setSubmitted(true);

            // // Show "View all Vehicle Models" button
            // const viewAllBtn = document.createElement('a');
            // viewAllBtn.href = 'http://localhost:3000/vehicle-models';
            // viewAllBtn.className = 'btn btn-primary mt-3';
            // viewAllBtn.textContent = 'View all Vehicle Models';
            // document.getElementById('create-vehicle-model-form').appendChild(viewAllBtn);
        } else {
            alert('Error creating vehicle');
        }
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form id="create-location-form" onSubmit={handleFormSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                                value={name}
                                onChange={(event) => setName(event.target.value)}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Picture URL"
                                required
                                type="text"
                                name="picture_url"
                                id="picture_url"
                                className="form-control"
                                value={pictureUrl}
                                onChange={(event) => setPictureUrl(event.target.value)}
                            />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select
                                required
                                name="manufacturer"
                                id="manufacturer"
                                className="form-select"
                                value={selectedManufacturer}
                                onChange={(event) => setSelectedManufacturer(event.target.value)}
                            >
                                <option value="">Select a Manufacturer</option>
                                {manufacturers.map((manufacturer) => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}

                            </select>
                        </div>
                        <button className="btn btn-primary" type="submit">
                            Create
                        </button>
                    </form>
                </div>
            </div >
        </div >
    );
}

export default VehicleModelForm;
