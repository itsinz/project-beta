// import React, { useEffect, useState } from 'react';

// function CreateTechnicianForm(props) {
//     const [tech_names, setTechnames] = useState([]);

//     const [tech_name, setTechName] = useState('');
//     const [employee_number, setEmployeeNumber] = useState('');





//     const handleEmployeeNumber = (event) => {
//         const value = event.target.value;
//         setEmployeeNumber(value);
//     }
//     const handleTechNameChange = (event) => {
//         const value = event.target.value;
//         setTechName(value);
//     }


//     const handleSubmit = async (event) => {
//         event.preventDefault();

//         const data = {};
//         data.employee_number = employee_number;
//         data.tech_name = tech_name;


//         const AppointmentUrl = 'http://localhost:8080/api/technicians/';
//         const fetchConfig = {
//           method: "post",
//           body: JSON.stringify(data),
//           headers: {
//             'Content-Type': 'application/json',
//           },
//         };

//         const response = await fetch(AppointmentUrl, fetchConfig);
//         if (response.ok) {
//             const technicians = await response.json()

//             setEmployeeNumber('');
//             setTechName('');


//         }
//     };


//     const fetchData = async () => {
//         const url = 'http://localhost:8080/api/appointments/';

//         const response = await fetch(url);

//         if (response.ok) {
//             const data = await response.json();
//             setTechnames(data.tech_names);
//         }
//     }


//     useEffect(() => {
//         fetchData();
//     }, []);

//     return (
//         <div className="row">
//             <div className="offset-3 col-6">
//                 <div className="shadow p-4 mt-4">
//                     <h1>Enter a technician</h1>
//                     <form id="enter-a-technician" onSubmit={handleSubmit}>
//                         <div className="form-floating mb-3">
//                             <input placeholder="tech_name" onChange={handleTechNameChange} value={tech_name} required type="text" name="tech_name" id="tech_name" className="form-control" />
//                             <label htmlFor="tech_name">Technician Name</label>
//                         </div>
//                         <div className="form-floating mb-3">
//                             <input placeholder="employee_number" onChange={handleEmployeeNumber} value={employee_number} required type="text" name="employee_number" id="employee_number" className="form-control" />
//                             <label htmlFor="employee_number">Employee ID</label>
//                         </div>
//                         <button className="btn btn-primary">Create</button>
//                     </form>
//                 </div>
//             </div>
//         </div>
//     );
// }

// export default CreateTechnicianForm;


import React, { useEffect, useState } from 'react';

function CreateTechnicianForm(props) {
    const [tech_names, setTechnames] = useState([]);

    const [tech_name, setTechName] = useState('');
    const [employee_number, setEmployeeNumber] = useState('');


    const handleEmployeeNumber = (event) => {
        const value = event.target.value;

        if (/^\d*$/.test(value)) {
            setEmployeeNumber(value);
        } else {
            setEmployeeNumber('');
        }
    };

    const handleTechNameChange = (event) => {
        const value = event.target.value;
        setTechName(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.employee_number = employee_number;
        data.tech_name = tech_name;


        const AppointmentUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(AppointmentUrl, fetchConfig);
        if (response.ok) {
            const technicians = await response.json()

            setEmployeeNumber('');
            setTechName('');


        }
    };


    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnames(data.tech_names);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Enter a technician</h1>
                    <form id="enter-a-technician" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="tech_name" onChange={handleTechNameChange} value={tech_name} required type="text" name="tech_name" id="tech_name" className="form-control" />
                            <label htmlFor="tech_name">Technician Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="employee_number" onChange={handleEmployeeNumber} value={employee_number} required type="text" name="employee_number" id="employee_number" className="form-control" />
                            <label htmlFor="employee_number">Employee ID (Numbers Only)</label>
                            {isNaN(employee_number) && (
                                <div className="text-danger mb-3">
                                    Please enter a valid employee ID.
                                </div>
                            )}
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateTechnicianForm;
