import React, { useEffect, useState } from 'react';

function ServiceAppointmentForm() {
    const [customer_vins, setCustomerVins] = useState([]);
    const [customer_vin, setCustomerVin] = useState('');
    const [customer_name, setCustomerName] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    // const [is_vip, setIsVIP] = useState('');
    const [technician, setTechnician] = useState('');
    const [vinError, setVinError] = useState('');


    const handleVINChange = (event) => {
        const value = event.target.value;
        if (value.length > 17) {
            setVinError('VIN should not exceed 17 characters.');
        } else {
            setVinError('');
            setCustomerVin(value);
        }
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.customer_vin = customer_vin;
        data.customer_name = customer_name;
        data.date = date;
        data.time = time;
        data.reason = reason;
        data.technician = technician;

        const AppointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(AppointmentUrl, fetchConfig);
        if (response.ok) {
            const appointment = await response.json()

            setCustomerVin('');
            setCustomerName('');
            setDate('');
            setTime('');
            setReason('');
            setTechnician('');
        }
    };


    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomerVins(data.customer_vins);
        }
    }



    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Enter a service appointment</h1>
                    <form id="enter-a-service-appointment" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                            <input placeholder="customer_vin" onChange={handleVINChange} value={customer_vin} required type="text" name="customer_vin" id="customer_vin" className="form-control" />
                            <label htmlFor="customer_vin">VIN</label>
                            {vinError && <div className="text-danger">{vinError}</div>}
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Customer_name" onChange={handleNameChange} value={customer_name} required type="text" name="customer_name" id="customer_name" className="form-control" />
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Date"onChange={handleDateChange} value={date} required type="text" name="date" id="date" className="form-control" />
                            <label htmlFor="date">Date (XXXX-XX-XX)</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Time" onChange={handleTimeChange} value={time} required type="text" name="time" id="time" className="form-control" />
                            <label htmlFor="time">Time (00:00:00)</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Reason" onChange={handleReasonChange} value={reason} required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Technician" onChange={handleTechnicianChange} value={technician} required type="text" name="technician" id="technician" className="form-control" />
                            <label htmlFor="technician">Technician ID (Number Only) </label>
                        </div>
                        <div className="mb-3">
                        {/* <select required name="technician" id="technician" className="form-select">
                            <option value="">Choose a technician</option>
                            {automobiles && automobiles.map((model) => (
                            <option key={model.id} value={model.id}>{model.name} ({model.manufacturer.name})</option>
                            ))}
                        </select> */}
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ServiceAppointmentForm;
