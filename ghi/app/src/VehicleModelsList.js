import React, { useState, useEffect } from 'react';

function VehicleModelList(props) {
    const [vehicleModels, setVehicleModels] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => setVehicleModels(data.models));
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Picture URL</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {vehicleModels.map((model) => (
                    <tr key={model.id}>
                        <td>{model.name}</td>
                        <td><img src={model.picture_url} alt={model.name} width="100px" /></td>
                        <td>{model.manufacturer.name}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default VehicleModelList;
