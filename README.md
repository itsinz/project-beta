# 🚙 CarCar Overview 🚗

CarCar is an application for managing different aspects of an automobile dealership, specifically its inventory, service center, and sales. This project includes microservices built using Django and a front-end application built using React.

<br>

**👨🏻‍💻👩🏻‍💻 Team**:

* Kurt - 🔧Services
* Sinlin - 💵 Sales




<br>

# 🏁 How to get started

**Step by step in your terminal:**

<br>

Step 1: Make a copy of the project in your repository
```bash
git clone https://gitlab.com/itsinz/project-beta.git
```
<br>

Step 2: Type this in your terminal to run docker
```bash
docker volume create beta-data
docker-compose build
docker-compose up
```
<br><br>

# 🗺️ The project set up
The image below gives an overview of the project from backend to frontend for the respective bounded contexts. Connected to the APIs, it fetches data needed to serve the different frontend views, allowing users to do different tasks available on the CarCar site.
<br><br>

**Service Tasks:** <br>
- Add Technician
- Add a Service Appointment Form
- View Service Appointment List
- View Service History by VIN

**Sales Tasks** <br>
- Add a Sales Person
- Add a Customer
- Create Sales Record
- List all Sales View
- List Sales History bt Sales Person

**Inventory Tasks** <br>
- Add manufacturer
- Add Vehicle Model
- Add Automobile
- View Manufacturer List
- View Vehicle List
- View Automobile List <br>
<br>

![ProjectImage](CarCarImage.png)

<br>

# 🪧 Ports for each of the microservices

Front-end application: http://localhost:3000
<br>
Inventory API: http://localhost:8100
<br>
Service API: http://localhost:8080
<br>
Sales API: http://localhost:8090

<br>

## CarCar Value Object

![ValueObjectImaget](ValueObjects.png)

- **AutomobileV0 Model** : The Sales and Services bounded context needs the Automobile data from the Inventory bounded context. It copies over the attributes of Automobile Model. The only need to know if the VIN number can be found in the Inventory Model, allowing them to carry out respective Sales and Service Task (Record a sale / See if the customer gets a VIP treatment during servicing).


<br>

# 🗂️ Inventory API Routes
<br>

From Insomnia and your browser, you can access the respective **Manufacturers, Vehicles and Automobile** API endpoints at the following URLs:

<br>

## 🔩 Manufacturers

| Action | Method | URL |
| --- | --- |  --- |
|  List manufacturers| `GET` | http://localhost:8100/api/manufacturers/ |
|  Create a manufacturers| `POST` | http://localhost:8100/api/manufacturers/ |
|  Get a specific manufacturer | `GET` | http://localhost:8100/api/manufacturers/:id/ |
|  Update a specific manufacturer | `PUT` | http://localhost:8100/api/manufacturers/:id/ |
|  Delete a specific manufacturer | `DELETE` | http://localhost:8100/api/manufacturers/:id/ |


<br>

**Create manufacturer input**
```bash
{
  "name": "Chrysler"
}
```
**Get manufacturer output**
```bash
{
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Mercedez"
		}
```

## 🚘 Vehicles

| Action | Method | URL |
| --- | --- |  --- |
|  List vehicle models | `GET` | http://localhost:8100/api/models/ |
|  Create a vehicle models | `POST` | http://localhost:8100/api/models/ |
|  Get a specific vehicle model | `GET` | http://localhost:8100/api/models/:id/ |
|  Update a specific vehicle model | `PUT` | http://localhost:8100/api/models/:id/ |
|  Delete a specific vehicle model | `DELETE` | http://localhost:8100/api/models/:id/ |

<br>

**Create vehicle model input**
```bash
{
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Mercedez"
		}
```
**Get vehicle model output**
```bash
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```



<br>

## 🚗 Automobile

| Action | Method | URL |
| --- | --- |  --- |
|  List automobile| `GET` | http://localhost:8100/api/automobiles/ |
|  Create automobile| `POST` | http://localhost:8100/api/automobiles/ |
|  Get a specific automobile | `GET` | http://localhost:8100/api/automobiles/:id/ |
|  Update a specific automobile | `PUT` | http://localhost:8100/api/automobiles/:id/ |
|  Delete a specific automobile | `DELETE` | http://localhost:8100/api/automobiles/:id/ |


<br>

**Create automobile input**
```bash
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
**Get automobile output**
```bash
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			}
		}
```



## Inventory Value Objects
- **Colour**: The color of a car can also be considered as a value object since it does not have a unique identity, but rather a characteristic that can be assigned to multiple cars.
- **Year**: The year of a car can also be considered as a value object since it does not have a unique identity, and gets assigned to multiple cars as characteristics.

<br><br>


# 💵 Sales microservice


**Models and integration with inventory microservice**
<br>
The following models (and their respective attributes) are used for Sales microservice, with AutomobileV0 as a value object from the inventory microservice so we know if a car is available for sale.

- class AutomobileV0 (import_href, color, year, vin, available) <br>
- class Customer (name, phone, address)
- class SalesPerson (name, employee_number)
- class Sales (automobile, sales_person, customer, sale_price)


<br>



# 🗂️ Sales API Routes
<br>

From Insomnia and your browser, you can access the respective **Sales Person, Customer, Sales** API endpoints at the following URLs:

<br>

## 🕺🏿 Sales Person

| Action | Method | URL |
| --- | --- |  --- |
|  List Sales Person| `GET` | http://localhost:8090/api/sales/ |
|  Create Sales Person | `POST` | http://localhost:8090/api/sales/ |



<br>

**Create Sales Person input**
```bash
{
    "name": "Bryan",
    "employee_number": 22
}
```
**Get Sales Person output**
```bash
{
		"name": "Bryan",
		"employee_number": 22,
		"id": 1
	},
```

<br>

## 🙂 Customer

| Action | Method | URL |
| --- | --- |  --- |
|  List customers | `GET` | http://localhost:8090/api/customer/ |
|  Create customer | `POST` | http://localhost:8090/api/customer/ |


<br>

**Create vehicle model input**
```bash
{
    "name": "Jane",
    "phone": "+1 202-918-2132",
    "address": "Pixir Drive"
}
```
**Get vehicle model output**
```bash
{
    "name": "Jane",
    "phone": "+1 202-918-2132",
    "address": "Pixir Drive",
    "id": 1
}
```



<br>

## 💵 Sales

| Action | Method | URL |
| --- | --- |  --- |
|  List automobile| `GET` | http://localhost:8090/api/sales/ |
|  Create automobile| `POST` | http://localhost:8090/api/sales/ |



<br>

**Create Sales input**
```bash
{
  "automobile_vin": "ABC123",
  "sales_person_id": 2,
  "customer_id": 1,
  "sale_price": 23456
}
```
**Get Sales output**
```bash
{
		"href": "/api/sales/1/",
		"automobile": "ABC123",
		"sales_person": {
			"name": "Bryan",
			"employee_number": 22,
			"id": 1
		},
		"customer": {
			"name": "Jane",
			"phone": "+1 202-918-2132",
			"address": "Pixir Drive",
			"id": 1
		},
		"sale_price": "23456",
		"id": 2
	}
```



## Sales Value Objects
- **Sale Price**: The sale price of a car can also be considered as a value object since it does not have a unique identity, but rather a characteristic that can be assigned to multiple cars.
- **AuthomobileV0**: Contains attributes which can be assigned to multiple cars eg: colour, year, and the availability of the cars in the inventory.

<br><br>

# 🔧 Service microservice

**Models and integration with inventory microservice**
<br>
The following models (and their respective attributes) are used for Sales microservice, with AutomobileV0 as a value object from the inventory microservice so we know if a car is available for sale.

- class AutomobileV0 (import_href, color, year, vin, available, model) <br>
- class Technician (tech_name, employee_number)
- class Appointment (customer_name, date, time, reason, customer_vin, is_vip, automobile, technician)


<br>



# 🗂️ Services API Routes
<br>

From Insomnia and your browser, you can access the respective **Technicians, Appointments** API endpoints at the following URLs:

<br>

## ⚒️ Technicians
<br>

KNOWN ISSUE - Technician ID/Employee Number:

When inputting the “employee ID” in the “Create service appointment” form, it should be the employee ID at creation in Django, of the number in the order they were created, not the “employee number” (employee_number)

<br>

| Action | Method | URL |
| --- | --- |  --- |
|  List Technician| `GET` | http://localhost:8080/api/technicians/ |
|  Create Technician | `POST` | http://localhost:8080/api/technicians/ |
|  Get a specific Technician| `GET` | http://localhost:8080/api/technicians/:id/ |
|  Delete a technician | `DELETE` | http://localhost:8080/api/technicians/:id/ |





<br>

**Create Technician input**
```bash
{
	"tech_name": "Tom",
	"employee_number": 100
}

```
**Get Technician output**
```bash
{
	"technician": {
		"tech_name": "John",
		"employee_number": 1,
		"id": 1
	}
}
```

<br>

## 📅 Appointments

| Action | Method | URL |
| --- | --- |  --- |
|  List service appointments| `GET` | http://localhost:8080/api/appointments/ |
|  Create service appointments | `POST` | http://localhost:8080/api/appointments/ |
|  Get a specific service appointment| `GET` | http://localhost:8080/api/appointments/:id/ |
|  Delete a service appointment | `DELETE` | http://localhost:8080/api/appointments/:id/ |
<br>

**Create Appointment input**
```bash
{
	"customer_name": "Ted",
	"date": "1986-10-09",
	"time": "06:00:00",
	"technician": 5,
	"reason": "Oil Change",
	"customer_vin": "CCH1F5NBFANf90ft5"
}
```
**Get Appointment output**
```bash
{
	"customer_name": "Ted",
	"date": "1986-10-09",
	"time": "06:00:00",
	"reason": "Oil Change",
	"customer_vin": "CCH1F5NBFANf90ft5",
	"is_vip": true,
	"technician": "Tom",
	"id": 1
}
```

<br><br>

## Service Value Objects
- **VIP Status**: The VIP status of a customer can be considered as a value object since it does not have a unique identity, but rather a characteristic that can be assigned to multiple customers. This is derived from matching VIN on AuthomobileV0 to existing VIN of customer's car.

<br><br>
